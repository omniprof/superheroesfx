package com.cejv416.superheroesfx;

import com.cejv416.superheroesfx.presentation.SuperHeroesUI_06;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;

/**
 * This class is where this application begins
 *
 * @author Ken Fogel
 */
public class SuperHerosFX_06 extends Application {

    private SuperHeroesUI_06 superUI;

    /**
     * Where the program begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * The required start method calls upon the start method in the
     * SuperHeroesUI class
     *
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        superUI.start(stage);
    }

    /**
     * Rather than a constructor, a class that extends Application uses an init
     * method.
     */
    @Override
    public void init() {
        superUI = new SuperHeroesUI_06();
    }

    /**
     * Overriding stop() ensures that this code will be carried out when the
     * program ends.
     */
    @Override
    public void stop() {
        System.out.println("Stage is closing");
    }
}
