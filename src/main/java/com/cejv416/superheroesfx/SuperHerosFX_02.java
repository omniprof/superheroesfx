
package com.cejv416.superheroesfx;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * This class is where this application begins
 * 
 * @author Ken Fogel
 */
public class SuperHerosFX_02  extends Application{
    
    /**
     * Where the program begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * The required start method calls upon the start method in the
     * SuperHeroesUI class
     *
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
