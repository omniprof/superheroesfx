package com.cejv416.superheroesfx.presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Ken Fogel
 */
public class SuperHeroesUI_03 {

    private Stage stage;
    private final TextField idTextField;
    private final TextField fullNameTextField;
    private final TextField superNameTextField;
    private final TextField wageTextField;
    
    /*****************************/
    /* Create the User Interface */
    /*****************************/

    /** 
     * Default Constructor instantiates the text fields objects
     */
    public SuperHeroesUI_03() {
        idTextField = new TextField();
        fullNameTextField = new TextField();
        superNameTextField = new TextField();
        wageTextField = new TextField();
    }

    /**
     * Set up the Scene and adds it to the Stage
     * 
     * @param stage 
     */
    public void start(Stage stage) {
        this.stage = stage;

        // Set window's title
        stage.setTitle("Super Hero Wages");

        Parent root = createLayout();
        Scene scene = new Scene(root, 600, 400);
        // This assumes that there is a folder named /src/main/resources
        scene.getStylesheets().add(getClass().getResource("/styles/Super.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Create all the components and place them in a GridPane
     * 
     * @return a GridPane
     */
    private GridPane createLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Super Hero Wage DB");
        scenetitle.setId("super-text");
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userNameLabel = new Label("ID:");
        grid.add(userNameLabel, 0, 1);
        grid.add(idTextField, 1, 1);

        Label fullNameLabel = new Label("Full Name:");
        grid.add(fullNameLabel, 0, 2);
        grid.add(fullNameTextField, 1, 2);

        Label superNameLabel = new Label("Super Name:");
        grid.add(superNameLabel, 0, 3);
        grid.add(superNameTextField, 1, 3);

        Label wageLabel = new Label("Wage:");
        grid.add(wageLabel, 0, 4);
        grid.add(wageTextField, 1, 4);

        Button clearButton = new Button("Clear");
        Button saveButton = new Button("Save");
        Button findButton = new Button("Find");
        Button exitButton = new Button("Exit");
        
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.CENTER);
        hbBtn.getChildren().add(clearButton);
        hbBtn.getChildren().add(saveButton);
        hbBtn.getChildren().add(findButton);
        hbBtn.getChildren().add(exitButton);
        grid.add(hbBtn, 0, 5, 2, 1);

        return grid;
    }
}
