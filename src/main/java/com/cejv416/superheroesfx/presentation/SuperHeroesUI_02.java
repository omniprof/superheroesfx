package com.cejv416.superheroesfx.presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Ken Fogel
 */
public class SuperHeroesUI_02 {

    private Stage stage;

    /*****************************/
    /* Create the User Interface */
    /*****************************/

    /**
     * Set up the Scene and adds it to the Stage
     * 
     * @param stage 
     */
    public void start(Stage stage) {
        this.stage = stage;
        
       // Set window's title
        stage.setTitle("Super Hero Wages");

        Parent root = createLayout();
        stage.setScene(new Scene(root, 300, 250));
        stage.show();

        // Event when the window is closed by an external event such as by
        // clicking on Window exit decoration (the X in the title bar).
        // A Platform.exit() is implied but other tasks to carry out on exit
        // can be called here
        stage.setOnCloseRequest(event -> {
            System.out.println("Bye for now.");
        });        
    }
    
    /**
     * Create all the components and place them in a GridPane
     * 
     * @return a GridPane
     */
    private GridPane createLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        return grid;
    }
}
