package com.cejv416.superheroesfx.presentation;

import com.cejv416.superheroesfx.data.SuperBean;
import com.cejv416.superheroesfx.persistence.SuperDAO;
import com.cejv416.superheroesfx.persistence.SuperDAOImpl;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Ken Fogel
 */
public class SuperHeroesUI_05 {

    private Stage stage;
    private final TextField idTextField;
    private final TextField fullNameTextField;
    private final TextField superNameTextField;
    private final TextField wageTextField;
    private final SuperDAO superDAO;

    /** 
     * Default Constructor instantiates the text fields and the DAO objects
     */
    public SuperHeroesUI_05() {
        idTextField = new TextField();
        fullNameTextField = new TextField();
        superNameTextField = new TextField();
        wageTextField = new TextField();
        superDAO = new SuperDAOImpl();
    }

    /*****************************/
    /* Create the User Interface */
    /*****************************/

    /**
     * Set up the Scene and adds it to the Stage
     * 
     * @param stage 
     */
    public void start(Stage stage) {
        this.stage = stage;

        // Set window's title
        stage.setTitle("Super Hero Wages");

        Parent root = createLayout();
        Scene scene = new Scene(root, 600, 400);
        // This assumes that there is a folder named /src/main/resources
        scene.getStylesheets().add(getClass().getResource("/styles/Super.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Create all the components and place them in a GridPane
     * 
     * @return a GridPane
     */
    private GridPane createLayout() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Super Hero Wage DB");
        scenetitle.setId("super-text");
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userNameLabel = new Label("ID:");
        grid.add(userNameLabel, 0, 1);
        grid.add(idTextField, 1, 1);

        Label fullNameLabel = new Label("Full Name:");
        grid.add(fullNameLabel, 0, 2);
        grid.add(fullNameTextField, 1, 2);

        Label superNameLabel = new Label("Super Name:");
        grid.add(superNameLabel, 0, 3);
        grid.add(superNameTextField, 1, 3);

        Label wageLabel = new Label("Wage:");
        grid.add(wageLabel, 0, 4);
        grid.add(wageTextField, 1, 4);

        Button clearButton = new Button("Clear");
        clearButton.setOnAction(this::clearButtonHandler);

        Button saveButton = new Button("Save");
        saveButton.setOnAction(this::saveButtonHandler);

        Button findButton = new Button("Find");
        findButton.setOnAction(this::findButtonHandler);

        Button exitButton = new Button("Exit");
        exitButton.setOnAction(this::exitButtonHandler);
        
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.CENTER);
        hbBtn.getChildren().add(clearButton);
        hbBtn.getChildren().add(saveButton);
        hbBtn.getChildren().add(findButton);
        hbBtn.getChildren().add(exitButton);
        grid.add(hbBtn, 0, 5, 2, 1);

        return grid;
    }

    /******************/
    /* Event Handlers */
    /******************/

    /**
     * Event handler for the Clear Button
     *
     * @param e
     */
    private void clearButtonHandler(ActionEvent e) {
        idTextField.setText("-1");
        fullNameTextField.setText("");
        superNameTextField.setText("");
        wageTextField.setText("0");
    }

    /**
     * Event handler for the Save Button
     *
     * @param e
     */
    private void saveButtonHandler(ActionEvent e) {
        System.out.println("saveButtonHandler");
    }

    /**
     * Event handler for the Find Button
     *
     * @param e
     */
    private void findButtonHandler(ActionEvent e) {

        // Verify the form ID can be converted to an int
        int id;
        try {
            id = Integer.parseInt(idTextField.getText());
        } catch (NumberFormatException nfe) {
            invalidInputAlert(idTextField.getText(), "ID");
            return;
        }

        // Call upon the SuperDAO to retrieve the record with the specified ID
        SuperBean superData;
        try {
            superData = superDAO.findID(id);
        } catch (SQLException ex) {
            sqlErrorAlert(ex.getMessage());
            return;
        }

        if (superData.getId() == -1) {
            notFoundAlert(id);
        } else {
            fillForm(superData);
        }
    }

    /**
     * Event handler for the Exit Button
     *
     * @param e
     */
    private void exitButtonHandler(ActionEvent e) {
        Platform.exit();
    }

    /**********************************/
    /* Utility methods to fill a form */
    /**********************************/
    
    /**
     * Fill the form text fields with the contents of a SuperBean
     *
     * @param superData
     */
    private void fillForm(SuperBean superData) {
        idTextField.setText(superData.getId() + "");
        fullNameTextField.setText(superData.getFullName());
        superNameTextField.setText(superData.getSuperName());
        wageTextField.setText(superData.getWage().toString());
    }

    /**
     * Alert pop up dialogs
     */
    
    /**
     * Display an Alert box if a Super Hero cannot be found
     *
     * @param badValue
     */
    private void notFoundAlert(int badValue) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Super Hero Not Found");
        alert.setHeaderText("Super Hero Not Found");
        alert.setContentText("There is no Super Hero with an ID of " + badValue);

        alert.showAndWait();
    }

    /**
     * Display an Alert box if a String cannot be converted to a number
     *
     * @param badValue
     * @param fieldName
     */
    private void invalidInputAlert(String badValue, String fieldName) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Invalid Input");
        alert.setHeaderText("Invalid Input");
        alert.setContentText("The number, " + badValue + ", you entered in " + fieldName + " is in an invalid format.");

        alert.showAndWait();
    }

    /**
     * Display an Alert box if an SQL error is thrown
     *
     * @param badValue
     */
    private void sqlErrorAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("SQL Error");
        alert.setHeaderText("SQL ERROR");
        alert.setContentText("SQL Error message " + message);

        alert.showAndWait();
    }

}
