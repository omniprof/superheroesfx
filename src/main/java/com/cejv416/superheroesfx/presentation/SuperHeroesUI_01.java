package com.cejv416.superheroesfx.presentation;

import javafx.stage.Stage;

/**
 *
 * @author Ken Fogel
 */
public class SuperHeroesUI_01 {

    private Stage stage;

    /*****************************/
    /* Create the User Interface */
    /*****************************/

    /**
     * Set up the Scene and add it to the Stage
     * 
     * @param stage 
     */
    public void start(Stage stage) {
        this.stage = stage;
    }
}
