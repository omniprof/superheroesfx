
package com.cejv416.superheroesfx;

/**
 * This class is where this application begins
 * 
 * @author Ken Fogel
 */
public class SuperHerosFX_01  {

    /**
     * Where the program begins
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Super Heroes Wage DB");
    }
}
