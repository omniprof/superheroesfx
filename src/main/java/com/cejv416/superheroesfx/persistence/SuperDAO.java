package com.cejv416.superheroesfx.persistence;

import com.cejv416.superheroesfx.data.SuperBean;
import java.sql.SQLException;

/**
 * Interface for CRUD methods
 *
 * @author Ken Fogel
 */
public interface SuperDAO {


    // Create
    public int create(SuperBean superData) throws SQLException;

    // Read
    public SuperBean findID(int id) throws SQLException;

    // Update
    public int update(SuperBean superData) throws SQLException;

    // Delete
    public int delete(int ID) throws SQLException;
}
