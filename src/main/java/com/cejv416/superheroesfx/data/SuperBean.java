package com.cejv416.superheroesfx.data;

import java.math.BigDecimal;

/**
 *
 * @author Ken Fogel
 */
public class SuperBean {

    private int id;
    private String fullName;
    private String superName;
    private BigDecimal wage;

    public SuperBean(int id, String fullName, String superName, BigDecimal wage) {
        this.id = id;
        this.fullName = fullName;
        this.superName = superName;
        this.wage = wage;
    }

    public SuperBean() {
        id = -1;
        fullName = "";
        superName = "";
        wage = BigDecimal.ZERO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSuperName() {
        return superName;
    }

    public void setSuperName(String superName) {
        this.superName = superName;
    }

    public BigDecimal getWage() {
        return wage;
    }

    public void setWage(BigDecimal wage) {
        this.wage = wage;
    }

    @Override
    public String toString() {
        return "Superbean{" + "id=" + id + ", fullName=" + fullName + ", superName=" + superName + ", wage=" + wage + '}';
    }

}
